#!/bin/sh

KERNEL=/boot/vmlinuz-6.1.0-26-arm64
RAMDISK=/boot/initrd.img-6.1.0-26-arm64

zstdcat ${RAMDISK} > /tmp/initrd.cpio

gzip /tmp/initrd.cpio

HOME=$PWD ./bin/mkbootimg --kernel ${KERNEL} --ramdisk ${RAMDISK} --dtb /usr/lib/linux-image-6.1.0-26-arm64/rockchip/rk3399-rock-pi-4b.dtb --header_version 2 --cmdline "rootwait ro earlycon=uart8250,mmio32,0xfeb50000 console=ttyFIQ0 maxcpus=1 consoleblank=0 loglevel=7" -o /tmp/boot.img
