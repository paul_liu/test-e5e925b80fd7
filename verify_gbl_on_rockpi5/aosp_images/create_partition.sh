#!/bin/sh

DEV=/dev/sda
SGDISK="sudo /sbin/sgdisk"

#rm -f "${DEV}"
#touch "${DEV}"
#truncate -s 8G "${DEV}"


${SGDISK} --new=1:64M:+128M    --typecode=1:ef00 --change-name=1:esp --attributes=1:set:0 "${DEV}"
${SGDISK} --new=2:192M:+4M     --change-name=2:security "${DEV}"
${SGDISK} --new=3:196M:+4M     --change-name=3:uboot "${DEV}"
${SGDISK} --new=4:200M:+4M     --change-name=4:trust "${DEV}"
${SGDISK} --new=5:204M:+4M     --change-name=5:misc "${DEV}"
${SGDISK} --new=6:208M:+4M     --change-name=6:dtbo "${DEV}"
${SGDISK} --new=7:212M:+1M     --change-name=7:vbmeta "${DEV}"
${SGDISK} --new=8:213M:+42M    --change-name=8:boot_a "${DEV}"
${SGDISK} --new=9:255M:+42M    --change-name=9:boot_b "${DEV}"
${SGDISK} --new=10:297M:+123M  --change-name=10:recovery "${DEV}"
${SGDISK} --new=11:420M:+372M  --change-name=11:backup "${DEV}"
${SGDISK} --new=12:792M:+384M  --change-name=12:cache "${DEV}"
${SGDISK} --new=13:1176M:+16M  --change-name=13:metadata "${DEV}"
${SGDISK} --new=14:1192M:+1M   --change-name=14:baseparameter "${DEV}"
${SGDISK} --new=15:1193M:+5120M   --change-name=15:super "${DEV}"
${SGDISK} --new=16:6313M:+6M   --change-name=16:userdata "${DEV}"
