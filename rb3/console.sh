#!/bin/sh

DEV1="/dev/serial/by-id/usb-FTDI_FT230X_Basic_UART_DK0DHKP4-if00-port0"
DEV2="/dev/null"
DEV3="/dev/null"

DEV="/dev/null"

if [ -e $DEV1 ]; then
    DEV=$DEV1
elif [ -e $DEV2 ]; then
    DEV=$DEV2
elif [ -e $DEV3 ]; then
    DEV=$DEV3
fi

if [ $DEV = /dev/null ]; then
    echo "Didn't found any devices"
    exit 1
fi

exec minicom -D "$DEV" -C /tmp/minicom-log_`date +%F_%T`.txt -o -w
