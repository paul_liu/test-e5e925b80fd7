#!/bin/sh

uboot="/tmp/uboot-qemu-aarch64/u-boot"

cat > /tmp/gdb-command-01.txt <<EOF
target extended-remote :13333
p/x (*(struct global_data*)\$x18)->relocaddr
symbol-file
add-symbol-file ${uboot} \$1
EOF

gdb-multiarch --command=/tmp/gdb-command-01.txt "${uboot}"
