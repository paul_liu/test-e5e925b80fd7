#!/bin/sh

DEV=/dev/sdc
SGDISK="sudo /sbin/sgdisk"

#rm -f "${DEV}"
#touch "${DEV}"
#truncate -s 8G "${DEV}"

${SGDISK} --new=1:17M:+128M    --typecode=1:ef00 --change-name=1:esp --attributes=1:set:0 "${DEV}"
${SGDISK} --new=2:145M:+128M   --partition-guid=2:77036CD4-03D5-42BB-8ED1-37E5A88BAA37 --change-name=2:boot_a "${DEV}"
${SGDISK} --new=3:273M:+128M   --change-name=3:boot_b "${DEV}"
${SGDISK} --new=4:401M:+140M   --partition-guid=4:9FB61B5A-5245-47C2-9A59-DBE909F38497 --change-name=4:init_boot_a "${DEV}"
${SGDISK} --new=5:541M:+140M   --change-name=5:init_boot_b "${DEV}"
${SGDISK} --new=6:681M:+128M   --partition-guid=6:4FFF84FA-5269-43A8-9E36-25C566123B67 --change-name=6:vendor_boot_a "${DEV}"
${SGDISK} --new=7:809M:+128M   --change-name=7:vendor_boot_b "${DEV}"
${SGDISK} --new=8:937M:+2M     --partition-guid=8:EBBEADAF-22C9-E33B-8F5D-0E81686A68C7 --change-name=8:modemst1 "${DEV}"
${SGDISK} --new=9:939M:+2M     --partition-guid=9:0A288B1F-22C9-E33B-8F5D-0E81686A68C7 --change-name=9:modemst2 "${DEV}"
${SGDISK} --new=10:941M:+2M    --partition-guid=10:638FF8E2-22C9-E33B-8F5D-0E81686A68C7 --change-name=10:fsg "${DEV}"
${SGDISK} --new=11:943M:+1M    --partition-guid=11:57B90A16-22C9-E33B-8F5D-0E81686A68C7 --change-name=11:fsc "${DEV}"
${SGDISK} --new=12:944M:+1M    --partition-guid=12:82ACC91F-357C-4A68-9C8F-689E1B1A23A7 --change-name=12:misc "${DEV}"
${SGDISK} --new=13:945M:+64M   --partition-guid=13:649044CD-B22B-443F-8DF8-687EB85380A7 --change-name=13:metadata "${DEV}"
${SGDISK} --new=14:1009M:+4096M --partition-guid=14:88147DE5-14E6-43CC-9FE2-48F6B99CBB47 --change-name=14:super "${DEV}"
${SGDISK} --new=15:5105M:+128M --change-name=15:vbmeta "${DEV}"
${SGDISK} --new=16:5233M:0     --partition-guid=16:1B81E7E6-F50D-419B-A739-2AEEF8DA3337 --change-name=16:userdata "${DEV}"
