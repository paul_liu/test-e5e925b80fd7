#!/bin/sh

set -e

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"


S=/home/paulliu/upstream/u-boot
B=/tmp/uboot-qemu-aarch64
rm -rf "$B"
mkdir -p "$B"

export CROSS_COMPILE=aarch64-linux-gnu-

cd "$S"

make O="$B" qemu_arm64_defconfig
cat <<EOF > "${B}"/extraconfig
CONFIG_PROT_TCP=y
CONFIG_PROT_TCP_SACK=y
CONFIG_CMD_WGET=y
CONFIG_UDP_FUNCTION_FASTBOOT=y
CONFIG_TCP_FUNCTION_FASTBOOT=y
CONFIG_IPV6=y
CONFIG_VIRTIO_CONSOLE=y
CONFIG_FASTBOOT_BUF_ADDR=0x08000000
CONFIG_CMD_EFIDEBUG=y
CONFIG_DISTRO_DEFAULTS=y
CONFIG_CC_OPTIMIZE_FOR_DEBUG=y
CONFIG_LTO=n
EOF

./scripts/kconfig/merge_config.sh -O ${B} ${B}/.config ${B}/extraconfig

make O="$B"
