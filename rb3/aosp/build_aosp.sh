#!/bin/bash

S="aosp_build"

cd "$S"

./device/linaro/dragonboard/fetch-vendor-package.sh
source ./build/envsetup.sh
lunch db845c-trunk_staging-userdebug
make -j`nproc`
