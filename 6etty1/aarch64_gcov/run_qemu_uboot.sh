#!/bin/sh

UBOOT=$(realpath u-boot-mainline/out/u-boot/dist/u-boot.bin)

tmpdisk=$(mktemp)
truncate -s 64M "${tmpdisk}"

/usr/sbin/mkfs.vfat "${tmpdisk}"

tmphellofile=$(mktemp)
echo "Hi" > "${tmphellofile}"

mcopy -i "${tmpdisk}" "${tmphellofile}" ::/hello.txt

exec qemu-system-aarch64 \
     -name guest=cvd-1,debug-threads=on \
     -machine virt,gic-version=2,usb=off,dump-guest-core=off \
     -m size=4096M,maxmem=4100M \
     -overcommit mem-lock=off \
     -smp 2,cores=2,threads=1 \
     -uuid 699acfc4-c8c4-11e7-882b-5065f31dc101 \
     -no-user-config -no-shutdown \
     -rtc base=utc \
     -boot strict=on \
     -cpu max \
     -netdev user,id=net0,hostfwd=tcp::5554-:5554 \
     -device virtio-net-pci,mac=50:54:00:00:00:56,netdev=net0,id=net0-dev \
     -object rng-builtin,id=objrng0 \
     -device virtio-rng-pci-non-transitional,rng=objrng0,id=rng0,max-bytes=1024,period=2000 \
     -drive file="${tmpdisk}",format=raw,if=none,aio=threads,id=drive-virtio-disk0 \
     -device virtio-blk-pci,drive=drive-virtio-disk0,iommu_platform=true,disable-legacy=on \
     -object cryptodev-backend-builtin,id=cryptodev0 \
     -device virtio-crypto-pci,id=crypto0,cryptodev=cryptodev0 \
     -device virtio-iommu-pci \
     -nographic \
     -device virtio-serial-pci,id=virtio-serial0 \
     -chardev socket,id=charconsole0,host=127.0.0.1,port=10023,telnet=on,server,nowait \
     -device virtconsole,chardev=charconsole0,id=console0 \
     -bios "${UBOOT}"

