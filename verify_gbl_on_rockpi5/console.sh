#!/bin/sh

DEV1="/dev/serial/by-id/usb-FTDI_6001-if00-port0"
DEV2="/dev/serial/by-id/usb-0403_6001-if00-port0"
DEV3="/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0"
DEV4="/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0"

DEV="/dev/null"

if [ -e $DEV1 ]; then
    DEV=$DEV1
elif [ -e $DEV2 ]; then
    DEV=$DEV2
elif [ -e $DEV3 ]; then
    DEV=$DEV3
elif [ -e $DEV4 ]; then
    DEV=$DEV4
fi

if [ $DEV = /dev/null ]; then
    echo "Didn't found any devices"
    exit 1
fi

exec minicom -D "$DEV" -C /tmp/minicom-log_`date +%F_%T`.txt -o -w -b 1500000
