#!/bin/sh

mkdir -p aosp
cd aosp

repo init -u https://github.com/radxa/manifests.git -b Android11_Radxa_rk11 -m rockchip-r-release.xml
repo sync -d --no-tags -j1
