#!/bin/sh

sshpass -p cuttlefish ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${TARGETSSH} 'rm -rf paulliu; mkdir -p paulliu'
sshpass -p cuttlefish scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" cvd-host_package.tar.gz ${TARGETSSH}:/home/vsoc-01/paulliu/
sshpass -p cuttlefish scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" aosp_cf_arm64*_phone-*.zip ${TARGETSSH}:/home/vsoc-01/paulliu/
sshpass -p cuttlefish ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${TARGETSSH} 'cd paulliu; tar -xvf cvd-host_package.tar.gz'
sshpass -p cuttlefish ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${TARGETSSH} 'cd paulliu; unzip aosp_cf_arm64*_phone-*.zip'
