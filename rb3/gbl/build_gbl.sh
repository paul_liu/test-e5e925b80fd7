#!/bin/sh

if [ -d gblefi ]; then
    cd gblefi
fi

./tools/bazel run //bootable/libbootloader:gbl_efi_dist --extra_toolchains=@gbl//toolchain:all --sandbox_debug --verbose_failures

