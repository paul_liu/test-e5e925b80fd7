#!/bin/sh

git clone https://github.com/tianocore/edk2.git
cd edk2
git submodule update --init --recursive
