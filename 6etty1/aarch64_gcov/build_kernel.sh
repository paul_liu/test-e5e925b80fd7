#!/bin/sh

export ANDROID_KERNEL_BUILD_ROOT="$(pwd)"

tools/bazel run --kasan --gcov //common-modules/virtual-device:virtual_device_aarch64_dist
