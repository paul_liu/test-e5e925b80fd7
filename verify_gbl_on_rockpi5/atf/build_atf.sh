#!/bin/sh

set -e

make realclean
make CROSS_COMPILE=aarch64-linux-gnu- PLAT=rk3588 LOG_LEVEL=50
