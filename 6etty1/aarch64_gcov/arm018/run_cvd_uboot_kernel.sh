#!/bin/sh

UBOOT=$(realpath u-boot-mainline/out/u-boot/dist/u-boot.bin)
KERNEL=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/Image.gz)
INITRAMFS=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/initramfs.img)

sshpass -p cuttlefish scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${UBOOT} ${TARGETSSH}:/home/vsoc-01/paulliu/my_uboot.bin
sshpass -p cuttlefish scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${KERNEL} ${TARGETSSH}:/home/vsoc-01/paulliu/my_Image
sshpass -p cuttlefish scp -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${INITRAMFS} ${TARGETSSH}:/home/vsoc-01/paulliu/my_initramfs.img

sshpass -p cuttlefish ssh -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" ${TARGETSSH} 'cd paulliu;HOME=$PWD ./bin/launch_cvd -report_anonymous_usage_stats=n -console=true -bootloader /home/vsoc-01/paulliu/my_uboot.bin -kernel_path /home/vsoc-01/paulliu/my_Image -initramfs_path /home/vsoc-01/paulliu/my_initramfs.img'
