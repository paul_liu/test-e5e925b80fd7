#!/bin/sh

W="/tmp/aw"

UBOOT=$(realpath u-boot-mainline/out/u-boot/dist/u-boot.bin)
KERNEL=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/Image.gz)
INITRAMFS=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/initramfs.img)

# On cloudtop, the home directory path is too long to create a UNIX sock.
# Thus we need to move our running path to /tmp instead.

rm -rf "${W}"
mkdir -p "${W}"

AOSP_BINARY=$(realpath aosp_cf_arm64*-*.zip)
CVDHOST_BINARY=$(realpath cvd-host_package-x86_64.tar.gz)

cd "${W}"; unzip ${AOSP_BINARY}; cd -
cd "${W}"; tar zxvf ${CVDHOST_BINARY}; cd -

cd "${W}"
HOME=$PWD ./bin/launch_cvd -report_anonymous_usage_stats=n -bootloader ${UBOOT} -console=true -kernel_path ${KERNEL} -initramfs_path ${INITRAMFS}
cd -
