#!/bin/sh

set -e

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"

B=../uboot-rockpi5
B=$(realpath "${B}")
rm -rf "$B"
mkdir -p "$B"

cd "$B"
git clone --depth 1 https://github.com/rockchip-linux/rkbin
cd -

export BL31=../trusted-firmware-a/build/rk3588/release/bl31/bl31.elf
cp -f "$BL31" "$B"
export BL31="$B"/bl31.elf

export ROCKCHIP_TPL=$(find "${B}/rkbin" -name "rk3588_ddr_lp4_2112MHz_lp5_2400MHz_v*.bin")

export CROSS_COMPILE=aarch64-linux-gnu-

make O="$B" rock5b-rk3588_defconfig
# CONFIG_MISC_INIT_R have to be disabled because we don't know why
# EFUSE is not working. And that will cause
# "initcall failed at call 0000000000a03bb4 (err=-1: Operation not permitted)"
# And due to the above CONFIG is disabled, we will not able to get the ethernet
# MAC address. Thus we need to turn on NET_RANDOM_ETHADDR
cat <<EOF > "${B}"/extraconfig
# CONFIG_MISC_INIT_R is not set
CONFIG_NET_RANDOM_ETHADDR=y
CONFIG_UDP_FUNCTION_FASTBOOT=y
CONFIG_USB_FUNCTION_FASTBOOT=y
CONFIG_FASTBOOT_BUF_ADDR=0x3000000
CONFIG_FASTBOOT_BUF_SIZE=0x5000000
CONFIG_FASTBOOT_FLASH=y
CONFIG_FASTBOOT_FLASH_MMC_DEV=0
CONFIG_DM_GPIO=y
CONFIG_CMD_GPIO=y
CONFIG_ROCKCHIP_EFUSE=y
CONFIG_FS_EXT4=y
CONFIG_NET_RANDOM_ETHADDR=y
EOF
./scripts/kconfig/merge_config.sh -O ${B} ${B}/.config ${B}/extraconfig

make O="$B"
