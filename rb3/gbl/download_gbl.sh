#!/bin/sh

KERNEL_MANIFEST_URL_DEFAULT="https://android.googlesource.com/kernel/manifest/"
KERNEL_MANIFEST_BRANCH_DEFAULT="uefi-gbl-mainline"

if [ x"$KERNEL_MANIFEST_URL" = x ]; then
    KERNEL_MANIFEST_URL="$KERNEL_MANIFEST_URL_DEFAULT"
fi

if [ x"$KERNEL_MANIFEST_BRANCH" = x ]; then
    KERNEL_MANIFEST_BRANCH="$KERNEL_MANIFEST_BRANCH_DEFAULT"
fi

mkdir -p gblefi

cd gblefi

repo init -u ${KERNEL_MANIFEST_URL} -b ${KERNEL_MANIFEST_BRANCH}
repo sync

cd -
