#!/bin/sh

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"

mkdir u-boot-mainline
cd u-boot-mainline
repo init -u https://android.googlesource.com/kernel/manifest -b u-boot-mainline
repo sync -j2 -q

