#!/bin/sh

KERNEL=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/Image)
RAMDISK=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/initramfs.img)
DTB=$(realpath common-android-mainline/out/virtual_device_aarch64/dist/fvp-base-revc.dtb)

cd /tmp/aw

HOME=$PWD ./bin/mkbootimg --kernel ${KERNEL} --ramdisk ${RAMDISK} --dtb ${DTB} --header_version 2 -o boot_new.img
