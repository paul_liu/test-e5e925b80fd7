#!/bin/sh

SELFPID=$$
renice 5 -p "$SELFPID"
#ionice -c 3 -p "$SELFPID"

tmpdisk="/tmp/uboot_qemu_disk.img"
tmpflash="/tmp/uboot_qemu_flash.img"
#uboot="/usr/lib/u-boot/qemu_arm64/u-boot.bin"
uboot="/tmp/uboot-qemu-aarch64/u-boot.bin"

if [ ! -e "${tmpdisk}" ]; then
    truncate -s 4096M "${tmpdisk}"

    # Partition the disk
    /sbin/sgdisk -a1 -n:1:64:8127 -t:1:8301 -c:1:bootloader ${tmpdisk}
    /sbin/sgdisk -a1 -n:2:8128:+64 -t:2:8301 -c:2:uboot_env ${tmpdisk}
    /sbin/sgdisk -n:3:8M:+4M -t:3:8301 -c:3:uboot ${tmpdisk}
    /sbin/sgdisk -n:4:12M:+4M -t:4:8301 -c:4:trust ${tmpdisk}
    /sbin/sgdisk -n:5:16M:+1M -t:5:8301 -c:5:misc ${tmpdisk}
    /sbin/sgdisk -n:6:17M:+128M -t:6:ef00 -c:6:efi -A:6:set:0 ${tmpdisk}
    /sbin/sgdisk -n:7:145M:0 -t:7:8305 -c:7:rootfs -A:7:set:2 ${tmpdisk}

    # format EFI partition
    system_partition="6"
    system_partition_start=$(partx -g -o START -s -n "${system_partition}" "${tmpdisk}" | xargs)
    system_partition_end=$(partx -g -o END -s -n "${system_partition}" "${tmpdisk}" | xargs)
    system_partition_num_sectors=$((${system_partition_end} - ${system_partition_start} + 1))
    system_partition_num_vfat_blocks=$((${system_partition_num_sectors} / 2))
    /sbin/mkfs.vfat -n SYSTEM -F 16 --offset=${system_partition_start} "${tmpdisk}" ${system_partition_num_vfat_blocks}
fi

# create Flash image for storing U-boot variables.
if [ ! -e "${tmpflash}" ]; then
    qemu-img create -f raw "${tmpflash}" 64M
fi

exec qemu-system-aarch64 -machine virt \
     -cpu cortex-a57 \
     -nographic \
     -netdev user,id=net0,hostfwd=tcp::5554-:5554 \
     -device virtio-net-pci,mac=50:54:00:00:00:56,netdev=net0,id=net0-dev \
     -object rng-builtin,id=objrng0 \
     -device virtio-rng-pci-non-transitional,rng=objrng0,id=rng0,max-bytes=1024,period=2000 \
     -drive file="${tmpdisk}",format=raw,if=none,aio=threads,id=drive-virtio-disk0 \
     -device virtio-blk-pci,drive=drive-virtio-disk0,iommu_platform=true,disable-legacy=on \
     -drive if=pflash,format=raw,index=1,file="${tmpflash}" \
     -object cryptodev-backend-builtin,id=cryptodev0 \
     -device virtio-crypto-pci,id=crypto0,cryptodev=cryptodev0 \
     -device virtio-iommu-pci \
     -device virtio-serial-pci,id=virtio-serial0 \
     -chardev socket,id=charconsole0,host=127.0.0.1,port=10023,telnet=on,server,nowait \
     -device virtconsole,chardev=charconsole0,id=console0 \
     -m 1G \
     -gdb tcp::13333 \
     -bios "${uboot}"
