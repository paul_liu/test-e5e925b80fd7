#!/bin/sh

TDIR=$(mktemp -d -t pbhookXXXXXX)

cat <<EOF > ${TDIR}/F30installdefaultpackages
sed -i 's/#deb-src/deb-src/g' /etc/apt/sources.list
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 update
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install devscripts
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install debhelper
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install config-package-dev git
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libcurl4-openssl-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libfmt-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libgflags-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libgoogle-glog-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libgtest-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libjsoncpp-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libprotobuf-c-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libprotobuf-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libssl-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libxml2-dev libz3-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install pkg-config
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install protobuf-compiler
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install uuid-dev
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libtinfo6
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install less emacs
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install git-buildpackage pristine-tar
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install quilt
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install libprotobuf-java protobuf-compiler-grpc-java-plugin
apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install architecture-is-64-bit dh-exec dh-python dh-sequence-bash-completion fonts-font-awesome fonts-glyphicons-halflings jdupes libasm-java libchecker-framework-java libcommons-collections3-java libcommons-compress-java libcommons-lang-java libcommons-logging-java libcommons-pool2-java libdd-plist-java libdiffutils-java libescapevelocity-java libgeronimo-annotation-1.3-spec-java libgoogle-api-client-java libgoogle-auth-java libgoogle-auto-common-java libgoogle-auto-service-java libgoogle-auto-value-java libgoogle-flogger-java libgoogle-http-client-java libgrpc-java libgrpc++-dev libjackson2-core-java libjacoco-java libjarjar-java libjava-allocation-instrumenter-java libjavapoet-java libjaxb-api-java libjcip-annotations-java libjcommander-java libjformatstring-java libjs-bootstrap libnanopb-dev libnetty-java libnetty-tcnative-java libopencensus-java libperfmark-java libproguard-java libprotoc-dev libreactive-streams-java librx-java libtruth-java libxz-java default-jdk-headless protobuf-compiler-grpc unzip velocity zip
#apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 install bazel-bootstrap
EOF
chmod a+x ${TDIR}/F30installdefaultpackages

sudo pbuilder login --bindmounts "$HOME/code" --hookdir "$TDIR"

