#!/bin/sh

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"

mkdir common-android-mainline
cd common-android-mainline

repo init -u https://android.googlesource.com/kernel/manifest/ -b common-android-mainline
repo sync -c -j2 -q
