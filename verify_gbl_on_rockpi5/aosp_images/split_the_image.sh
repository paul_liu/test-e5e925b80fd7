#!/bin/sh

IMAGEFILE="$1"
NUMOFPARTITIONS=$(partx -rgo NR -n -1:-1 "${IMAGEFILE}")

i=0
while [ $i -ne ${NUMOFPARTITIONS} ]; do
    i=$(($i+1))
    START1=$(partx -g -o START -s -n "$i" "${IMAGEFILE}")
    SECTORS1=$(partx -g -o SECTORS -s -n "$i" "${IMAGEFILE}")
    NAME1=$(partx -g -o NAME -s -n "$i" "${IMAGEFILE}")
    dd if="${IMAGEFILE}" of="${NAME1}".img bs=512 skip="${START1}" count="${SECTORS1}"
done
