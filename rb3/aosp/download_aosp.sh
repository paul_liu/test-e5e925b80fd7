#!/bin/sh

S="aosp_build"

if [ ! -d "$S" ]; then
    mkdir "$S"
    cd "$S"
    repo init -u https://android.googlesource.com/platform/manifest -b master
else
    cd "$S"
fi

repo sync -j`nproc`
