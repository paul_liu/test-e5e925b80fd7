#!/bin/sh

set +e

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"

set -e

B=../uboot_build_place
rm -rf "$B"
mkdir -p "$B"

B=$(realpath "${B}")

export CROSS_COMPILE=aarch64-linux-gnu-

cd "$S"

make O="$B" qcom_defconfig
#./tools/buildman/buildman -o "${B}" qcom
cat <<EOF > "${B}"/extraconfig
CONFIG_NET=y
CONFIG_CMD_BOOTP=y
CONFIG_CMD_DHCP=y
CONFIG_UDP_FUNCTION_FASTBOOT=y
CONFIG_USB_FUNCTION_FASTBOOT=y
CONFIG_FASTBOOT_BUF_ADDR=0xA0000000
CONFIG_FASTBOOT_BUF_SIZE=0x05000000
CONFIG_FASTBOOT_FLASH=y
CONFIG_FASTBOOT_FLASH_MMC_DEV=0
CONFIG_CMD_USB_MASS_STORAGE=y
CONFIG_DM_USB_GADGET=y
EOF

./scripts/kconfig/merge_config.sh -O ${B} ${B}/.config ${B}/extraconfig

make O="$B"

cd "$B"
if [ -e "./.bm-work/00/build/u-boot-nodtb.bin" ]; then
    cp -f ./.bm-work/00/build/u-boot-nodtb.bin .
fi
if [ -e "./.bm-work/00/build/dts/upstream/src/arm64/qcom/sdm845-db845c.dtb" ]; then
    cp -f "./.bm-work/00/build/dts/upstream/src/arm64/qcom/sdm845-db845c.dtb" .
fi
if [ -e "./dts/upstream/src/arm64/qcom/sdm845-db845c.dtb" ]; then
    cp -f ./dts/upstream/src/arm64/qcom/sdm845-db845c.dtb .
elif [ -e "./arch/arm/dts/dragonboard845c.dtb" ]; then
    cp -f ./arch/arm/dts/dragonboard845c.dtb ./sdm845-db845c.dtb
fi

gzip u-boot-nodtb.bin
cat u-boot-nodtb.bin.gz sdm845-db845c.dtb > u-boot-nodtb.bin.gz-dtb
mkbootimg --kernel u-boot-nodtb.bin.gz-dtb \
        --output boot.img --pagesize 4096 --base 0x80000000
